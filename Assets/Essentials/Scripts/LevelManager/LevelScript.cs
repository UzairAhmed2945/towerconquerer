using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;

public class LevelScript : MonoBehaviour
{
    [SerializeField] private SplineComputer m_PlayerSplineComputer;

    public SplineComputer PlayerSplineComputer { get { return m_PlayerSplineComputer; } }

    [SerializeField] private Material m_PathMat;
    [SerializeField] private Material m_FloorMat;
    [SerializeField] private Texture m_PathMaterialSprite;
    [SerializeField] private Animator m_DoorAnimator;


    [SerializeField] private PlayerScript m_Player;

    //[SerializeField] private EnemyScript[] m_Enemies;


    public GameObject Player;
    public GameObject Player_TransformPoint;
    public int PlayerDecreaseHealth;
    public int PlayerMaxHealth;
    public int FollowersDecreaseonArrowCollide;


    [Header("Environment")]
    [SerializeField] private Color m_FogColor;
    [SerializeField] private Color m_CameraColor;
    [SerializeField] private Color m_PathColor;
    [SerializeField] private Color m_FloorColor;

    [Header("Open/Close Objects")]
    [SerializeField] private GameObject[] m_OpenHandObjects;
    [SerializeField] private GameObject[] m_CloseHandObjects;




    private void OnEnable()
    {
        setLevelSplines();
        setLevelEnvironment();
    }

    void setLevelSplines()
    {
        m_Player.DecreaseHp = PlayerDecreaseHealth;
        m_Player.MaxHp = PlayerMaxHealth;
        m_Player.DecreaseFollowerCount = FollowersDecreaseonArrowCollide;
        m_Player.m_DoorAnimator = m_DoorAnimator;
        m_Player.SetPlayer(m_PlayerSplineComputer);

        for(int i =0; i< m_CloseHandObjects.Length; i++)
        {
            m_CloseHandObjects[i].SetActive(false);
        }

        for (int i = 0; i < m_OpenHandObjects.Length; i++)
        {
            m_OpenHandObjects[i].SetActive(true);
        }


    }

    void setLevelEnvironment()
    {
        RenderSettings.fogColor = m_FogColor;
        Camera.main.backgroundColor = m_CameraColor;
        m_FloorMat.color = m_FloorColor;
        m_PathMat.mainTexture = m_PathMaterialSprite;
        m_PathMat.color = m_PathColor;
        

    }
}
