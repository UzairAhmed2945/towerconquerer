using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    #region Data
    public LevelScript[] LevelScripts;
    public Camera m_Camera;
    public CameraBehaviour CameraBehaviour;

    

    public LevelScript CurrentLevel;
    #endregion Data
    #region Unity Initialization
    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += setLevel;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= setLevel;
    }

    private void Awake()
    {
        setLevel();
    }
    #endregion Unity Initialization


    void setLevel()
    {
        closeAllLevels();
        var levelNo = StorageManager.Instance.CurrentlevelNo;

        while (levelNo > LevelScripts.Length - 1)
        {
            levelNo -= LevelScripts.Length;
        }
        CameraBehaviour.CamMoveTarget = LevelScripts[levelNo].Player_TransformPoint;
        CameraBehaviour.LookAtTarget = LevelScripts[levelNo].Player.transform;
        LevelScripts[levelNo].gameObject.SetActive(true);
        CurrentLevel = LevelScripts[levelNo];


    }

    void closeAllLevels()
    {
        for (int i = 0; i < LevelScripts.Length; i++)
        {
            LevelScripts[i].gameObject.SetActive(false);
        }
    }

}
