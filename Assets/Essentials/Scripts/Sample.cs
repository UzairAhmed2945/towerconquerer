using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample : MonoBehaviour
{
    GameObject i_Object;
    public bool isSample = true;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) && !isSample)
        {
            i_Object =  PoolManager.Instance.Dequeue(ePoolType.Sample);
            i_Object.SetActive(true);
        }else if (Input.GetKeyDown(KeyCode.H))
        {
            if(isSample)
            PoolManager.Instance.EnQueue(ePoolType.Sample, this.gameObject);
        }
    }
}
