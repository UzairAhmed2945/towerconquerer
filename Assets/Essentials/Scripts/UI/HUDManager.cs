using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HUDManager : Singleton<HUDManager>
{
    #region Data
    [SerializeField] private ExtendedButton m_InputButton;
    #endregion Data

    [Space]
    [Header("GamePanels")]
    [SerializeField] private GameObject m_GameCompletePanel;
    [SerializeField] private GameObject m_GameFailPanel;
    [SerializeField] private GameObject m_GameStartPanel;

    [Space]
    [Header("GamePanelButtons")]
    [SerializeField] private ExtendedButton m_StartButton;
    [SerializeField] private ExtendedButton m_ResetButton;
    [SerializeField] private ExtendedButton m_NextButton;
    

    [Space]
    [Header("Coins")]

    [SerializeField] private TextMeshProUGUI m_CoinsText;

    [Space]
    [Header("Level No")]

    [SerializeField] private TextMeshProUGUI m_LevelNo;

    [Space]
    [Header("Vibration")]

    [SerializeField] private Image m_VibrationON;
    [SerializeField] private Image m_VibrationOFF;
    [SerializeField] private ExtendedButton m_VibrationButton;

    [Space]
    [Header("Vibration")]
    [SerializeField] private ExtendedButton m_SettingButton;
    [SerializeField] private GameObject m_SettingPanel;
    [SerializeField] private GameObject m_LevelSelectionPanel;
    [SerializeField] private ExtendedButton m_CrossButtonSettingPanel;
    [SerializeField] private ExtendedButton m_CrossButtonLevelSelectionPanel;
    [SerializeField] private ExtendedButton m_RestartLevel;
    [SerializeField] private ExtendedButton m_LevelSelectionButton;
    [SerializeField] private GameObject[] Locks;






    private void OnEnable()
    {
        m_InputButton.OnDownEvent += InputDown;
        m_InputButton.OnUpEvent += InputUp;
        m_StartButton.onClick.AddListener(OnClickStartButton);
        m_ResetButton.onClick.AddListener(ClickReset);
        m_NextButton.onClick.AddListener(ClickNext);
        GameManagerDelegate.OnGameReset += OnClickResetButton;
        GameManagerDelegate.OnGameFail += ShowGameFailPanel;
        GameManagerDelegate.OnGameComplete += ShowGameCompletePanel;
        m_VibrationButton.onClick.AddListener(OnClickVibration);
        m_SettingButton.onClick.AddListener(OnClickSettingButton);
        m_RestartLevel.onClick.AddListener(ResetLevel);
        m_LevelSelectionButton.onClick.AddListener(OnCLickLevelSelection);
        m_CrossButtonSettingPanel.onClick.AddListener(OnCloseSetttingPanel);
        m_CrossButtonLevelSelectionPanel.onClick.AddListener(OnCloseLevelSelectionPanel);



    }

    private void OnDisable()
    {
        m_InputButton.OnDownEvent -= InputDown;
        m_InputButton.OnUpEvent -= InputUp;
        m_StartButton.onClick.RemoveListener(OnClickStartButton);
        m_ResetButton.onClick.RemoveListener(ClickReset);
        m_NextButton.onClick.RemoveListener(ClickNext);
        GameManagerDelegate.OnGameReset -= OnClickResetButton;
        GameManagerDelegate.OnGameFail -= ShowGameFailPanel;
        GameManagerDelegate.OnGameComplete -= ShowGameCompletePanel;
        m_VibrationButton.onClick.RemoveListener(OnClickVibration);
        m_SettingButton.onClick.RemoveListener(OnClickSettingButton);
        m_RestartLevel.onClick.RemoveListener(ResetLevel);
        m_LevelSelectionButton.onClick.RemoveListener(OnCLickLevelSelection);
        m_CrossButtonSettingPanel.onClick.RemoveListener(OnCloseSetttingPanel);
        m_CrossButtonLevelSelectionPanel.onClick.RemoveListener(OnCloseLevelSelectionPanel);
    }

    private void Start()
    {
        m_CoinsText.text = PlayerPrefs.GetInt(nameof(eTagType.Coin), 0).ToString();
        if(StorageManager.Instance.Vibration == 0)
        {
            m_VibrationOFF.gameObject.SetActive(false);
            m_VibrationON.gameObject.SetActive(true);
        }
        else
        {
            m_VibrationOFF.gameObject.SetActive(true);
            m_VibrationON.gameObject.SetActive(false);
        }

        CheckLockNumber();
        m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
       


    }

    #region GameStates
    void OnClickStartButton()
    {

        StartCoroutine(ClickStartButton());
    }

    IEnumerator ClickStartButton()
    {
        m_GameStartPanel.SetActive(false);
        yield return new WaitForSeconds(0f);
        GameManager.Instance.Gamestart();

    }

    void OnClickResetButton()
    {

        StartCoroutine(ClickResetButton());
    }

    IEnumerator ClickResetButton()
    {
        yield return new WaitForSeconds(0.1f);
        if (m_GameFailPanel.activeSelf)
        {
            m_GameFailPanel.SetActive(false);
        }
        if (m_GameCompletePanel.activeSelf)
        {
            m_GameCompletePanel.SetActive(false);
        }
        //ClickReset();
        yield return new WaitForSeconds(0.1f);
        m_GameStartPanel.SetActive(true);

    }

    void ShowGameCompletePanel()
    {
        StartCoroutine(GameCompletePanel());
    }

    IEnumerator GameCompletePanel()
    {
        CheckLockNumber();
        yield return new WaitForSeconds(2f);
        m_GameCompletePanel.SetActive(true);
    }

    

    void ShowGameFailPanel()
    {
        StartCoroutine(GameFailPanel());
    }

    IEnumerator GameFailPanel()
    {
        yield return new WaitForSeconds(2f);
        m_GameFailPanel.SetActive(true);
    }

    void ClickReset()
    {
        GameManager.Instance.GameReset();
    }

    void ClickNext()
    {
        m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
        GameManager.Instance.GameReset();
    }

   
    #endregion GameStates
        
    #region InputRegion
    void InputDown(PointerEventData pointer)
    {
        InputManager.Instance.InputDown();
    }

    void InputUp(PointerEventData pointer)
    {
        InputManager.Instance.InputUp();
    }

    #endregion InputRegion

    #region Coins
    public void AddCoins()
    {
        int Coins = PlayerPrefs.GetInt(nameof(eTagType.Coin), 0);
        Coins++;
        PlayerPrefs.SetInt(nameof(eTagType.Coin), Coins);
        m_CoinsText.text = PlayerPrefs.GetInt(nameof(eTagType.Coin), 0).ToString();
    }
    #endregion Coins

    #region Vibration

    public void OnClickVibration()
    {
        if(StorageManager.Instance.Vibration == 1)
        {
            m_VibrationOFF.gameObject.SetActive(false);
            m_VibrationON.gameObject.SetActive(true);
            StorageManager.Instance.Vibration = 0;
        }
        else if(StorageManager.Instance.Vibration == 0)
        {
            m_VibrationOFF.gameObject.SetActive(true);
            m_VibrationON.gameObject.SetActive(false);
            StorageManager.Instance.Vibration = 1;
        }
    }
    #endregion Vibration

    #region Settings

    public void OnClickSettingButton()
    {
        Time.timeScale = 0;
        m_SettingPanel.SetActive(true);

    }

    public void OnCloseSetttingPanel()
    {
        Time.timeScale = 1;
        m_SettingPanel.SetActive(false);
    }

    public void OnCLickLevelSelection()
    {
        for (int i = 0; i < StorageManager.Instance.LockNumber; i++)
        {
            Locks[i].SetActive(false);
        }
        

        m_LevelSelectionPanel.SetActive(true);
    }

    public void OnCloseLevelSelectionPanel()
    {
        m_LevelSelectionPanel.SetActive(false);
    }

    public void OpenLevel(int ChapterNumber)
    {
        //if (StorageManager.Instance.CurrentlevelNo <= 4)
        //{
        //    StorageManager.Instance.CurrentlevelNo = levelNumber;
        //    m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
        //    GameManager.Instance.GameReset();

        //}
        //else
        //{
        //    StorageManager.Instance.CurrentlevelNo++;
        //    m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
        //    GameManager.Instance.GameReset();
        //}
        switch (ChapterNumber)
        {
            case 1:
                StorageManager.Instance.CurrentlevelNo = 0;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;
            case 2:
                StorageManager.Instance.CurrentlevelNo = 5;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;
            case 3:
                StorageManager.Instance.CurrentlevelNo = 10;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;
            case 4:
                StorageManager.Instance.CurrentlevelNo = 15;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;
            case 5:
                StorageManager.Instance.CurrentlevelNo = 20;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;
            case 6:
                StorageManager.Instance.CurrentlevelNo = 25;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;
            case 7:
                StorageManager.Instance.CurrentlevelNo = 30;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;
            case 8:
                StorageManager.Instance.CurrentlevelNo = 35;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;
            case 9:
                StorageManager.Instance.CurrentlevelNo = 40;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;
            case 10:
                StorageManager.Instance.CurrentlevelNo = 45;
                m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
                GameManager.Instance.GameReset();
                break;

        }


        m_LevelSelectionPanel.SetActive(false);
        m_SettingPanel.SetActive(false);
        Time.timeScale = 1f;
    }

    void NextLevel()
    {
        StorageManager.Instance.CurrentlevelNo++;
        m_LevelNo.text = "Level " + (StorageManager.Instance.CurrentlevelNo + 1).ToString();
        GameManager.Instance.GameReset();
    }
    void ResetLevel()
    {
        Time.timeScale = 1f;
        m_SettingPanel.SetActive(false);
        m_LevelSelectionPanel.SetActive(false);
        GameManager.Instance.GameReset();
    }


    public void CheckLockNumber()
    {
        if ((StorageManager.Instance.CurrentlevelNo > 4 && StorageManager.Instance.CurrentlevelNo <10) && StorageManager.Instance.LockNumber < 1)
        {
            StorageManager.Instance.LockNumber = 1;

        }
        else if ((StorageManager.Instance.CurrentlevelNo > 9 && StorageManager.Instance.CurrentlevelNo < 15) && StorageManager.Instance.LockNumber < 2)
        {
            StorageManager.Instance.LockNumber = 2;

        }
        else if ((StorageManager.Instance.CurrentlevelNo > 14 && StorageManager.Instance.CurrentlevelNo < 20) && StorageManager.Instance.LockNumber < 3)
        {
            StorageManager.Instance.LockNumber = 3;

        }
        else if ((StorageManager.Instance.CurrentlevelNo > 19 && StorageManager.Instance.CurrentlevelNo < 25) && StorageManager.Instance.LockNumber < 4)
        {
            StorageManager.Instance.LockNumber = 4;

        }
        else if ((StorageManager.Instance.CurrentlevelNo > 24 && StorageManager.Instance.CurrentlevelNo < 30) && StorageManager.Instance.LockNumber < 5)
        {
            StorageManager.Instance.LockNumber = 5;

        }
        else if ((StorageManager.Instance.CurrentlevelNo > 29 && StorageManager.Instance.CurrentlevelNo < 35) && StorageManager.Instance.LockNumber < 6)
        {
            StorageManager.Instance.LockNumber = 6;

        }
        else if ((StorageManager.Instance.CurrentlevelNo > 34 && StorageManager.Instance.CurrentlevelNo < 40) && StorageManager.Instance.LockNumber < 7)
        {
            StorageManager.Instance.LockNumber = 7;

        }
        else if ((StorageManager.Instance.CurrentlevelNo > 39 && StorageManager.Instance.CurrentlevelNo < 45) && StorageManager.Instance.LockNumber < 8)
        {
            StorageManager.Instance.LockNumber = 8;

        }
        else if ((StorageManager.Instance.CurrentlevelNo > 44 && StorageManager.Instance.CurrentlevelNo < 50) && StorageManager.Instance.LockNumber < 9)
        {
            StorageManager.Instance.LockNumber = 9;

        }
        else if (StorageManager.Instance.CurrentlevelNo >= 50)
        {
            StorageManager.Instance.CurrentlevelNo = 0;
        }
    }

    #endregion Settings



}

