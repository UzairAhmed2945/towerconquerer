using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowSimulation : MonoBehaviour
{
    public Vector3 Target;
    public float firingAngle = 45.0f;
    public float gravity = 9.8f;

    public Transform Projectile;
    public Transform myTransform;

    private Vector3 m_CurrentPosition;
    public Vector3 m_LastPosition;
    [SerializeField] private Transform m_Arrow;
    public float speed;
    public float Distance;
    public Transform ArrowpointTransform;
    

    public bool canMove;
    public bool canMoveFoward;

    void Awake()
    {
        
    }

    void Start()
    {
        
    }

    void OnEnable()
    {
        canMove = true;
        canMoveFoward = true;
        myTransform = transform;
        m_LastPosition = Projectile.position;
        ThrowerDelegate.OnThrowArrow += SimulateArrow;
        //ArrowpointTransform = GameManager.Instance.Player.ArrowPoint.transform;
        
    }

    

    void OnDisable()
    {
        canMove = false;
        canMoveFoward = false;
        ThrowerDelegate.OnThrowArrow -= SimulateArrow;
    }

    public void FixedUpdate()
    {
        if (canMove)
        {
            transform.LookAt(Target);

        }
        if (canMoveFoward)
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }

        var distance = Vector3.Distance(Target, transform.position);
        if (distance < Distance)
        {
            canMove = false;
        }

    }

    public void SimulateArrow(Vector3 e_Target , GameObject e_Arrow)
    {
        if (this.gameObject == e_Arrow)
        {
            Target = e_Target;
            //StartCoroutine(SimulateProjectile());
        }
    }

    IEnumerator SimulateProjectile()
    {
        // Short delay added before Projectile is thrown
        yield return new WaitForSeconds(0f);

        // Move projectile to the position of throwing object + add some offset if needed.
        Projectile.position = myTransform.position + new Vector3(0, 0, 0);

        // Calculate distance to target
        float target_Distance = Vector3.Distance(Projectile.position, Target);

        // Calculate the velocity needed to throw the object to the target at specified angle.
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = target_Distance / Vx;

        // Rotate projectile to face the target.
        Projectile.rotation = Quaternion.LookRotation(Target - Projectile.position);

        float elapse_time = 0;

        while (elapse_time < flightDuration)
        {
            Projectile.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);
            m_CurrentPosition = Projectile.position;
            var direction = m_CurrentPosition - m_LastPosition;
            m_Arrow.rotation = Quaternion.LookRotation(direction);
            m_LastPosition = m_CurrentPosition;
            elapse_time += Time.deltaTime;

            yield return null;
        }
    }
}
