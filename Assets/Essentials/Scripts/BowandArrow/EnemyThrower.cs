using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyThrower : MonoBehaviour
{
    #region Data
    [SerializeField] private Animator m_EnAnimator;
    [SerializeField] private GameObject m_Bow;
    private Quaternion m_EnemyInitialRotation;
    [SerializeField] private EnemyVision m_Vision;
    [SerializeField] private float m_TimeToshootmin;
    [SerializeField] private float m_TimeToshootmax;
    private IEnumerator throwarrow;
    private IEnumerator Ithrowarrow;
    private bool canShoot;
    [SerializeField] private LineRenderer m_lineRenderer;
    [SerializeField] private Vector3 m_DropPos;
    private bool islinerenderer;
    [SerializeField] private Transform m_Redlinepoint;


    #endregion Data

    #region Unity Initialization
    private void Start()
    {
        m_EnemyInitialRotation = this.transform.localRotation;
        
    }

    private void Update()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            if (m_Vision.isinRange == true && !canShoot)
            {
                Ithrowarrow = IenumThrowArrow();
                StartCoroutine(Ithrowarrow);
            }
            if (m_Vision.isinRange == true)
            {
                var lookPos = m_Vision.m_TriggerObject.transform.position - transform.position;
                lookPos.y = 0;
                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 12);
            }
        }

        if (islinerenderer)
        {
            m_lineRenderer.useWorldSpace = true;
            m_lineRenderer.SetPosition(1, m_Redlinepoint.position);
            m_lineRenderer.SetPosition(0, GameManager.Instance.Player.redLinePoint.transform.position);
        }
        else if (!islinerenderer)
        {
            m_lineRenderer.SetPosition(0, Vector3.zero);
            m_lineRenderer.SetPosition(1, Vector3.zero);
        }
    }

    private void OnEnable()
    {
        EnemyDelegate.OnChangeEnemy += OnGameNext;
        GameManagerDelegate.OnGameReset += OnReset;
        GameManagerDelegate.OnGameFail += OnGameFail;
        //EnemyDelegate.onChangeLineRenderer += ReachingGroundPoint;
        GameManagerDelegate.OnGameStart += GameStart;
    }

    private void OnDisable()
    {
        EnemyDelegate.OnChangeEnemy -= OnGameNext;
        GameManagerDelegate.OnGameReset -= OnReset;
        GameManagerDelegate.OnGameFail -= OnGameFail;
        //EnemyDelegate.onChangeLineRenderer -= ReachingGroundPoint;
        GameManagerDelegate.OnGameStart -= GameStart;
    }
    #endregion Unity Initialization

    #region Game States


    IEnumerator IenumThrowArrow()
    {
        canShoot = true;
        float timetoshoot = Random.Range(m_TimeToshootmin, m_TimeToshootmax);
        yield return new WaitForSeconds(timetoshoot);
        GameManager.Instance.Player.OpenDangerObject();
        yield return new WaitForSeconds(0.1f);
        m_EnAnimator.SetTrigger(nameof(eAnimStates.Throw));
        
        canShoot = false;

    }

    void ShootArrow()
    {
        //throwarrow = IThrowArrow();
        //StartCoroutine(throwarrow);
        //IThrowArrow();
        islinerenderer = true;
        
        Invoke(nameof(IThrowArrow), 1f);

    }

    void IThrowArrow()
    {
        //yield return new WaitForSeconds(0f);

        // yield return new WaitForSeconds(2f);

        islinerenderer = false;
        m_lineRenderer.SetPosition(0, Vector3.zero);
        m_lineRenderer.SetPosition(1, Vector3.zero);
        var Droppoint = PoolManager.Instance.Dequeue(ePoolType.DropPoint);
        m_DropPos = GameManager.Instance.Player.ArrowPoint.transform.position;
        Droppoint.transform.position = m_DropPos;
        Droppoint.SetActive(true);
        var arrow = PoolManager.Instance.Dequeue(ePoolType.Arrow);
        arrow.transform.position = m_Bow.transform.position;
        arrow.transform.rotation = m_Bow.transform.localRotation;
        arrow.SetActive(true);
        ThrowerDelegate.ThrowArrow(m_DropPos, arrow);
        
        
    }

    void OnGameNext()
    {
        canShoot = false;
        m_Vision.isinRange = false;
        m_Vision.m_TriggerObject = null;
        if (Ithrowarrow != null)
        {
            StopCoroutine(Ithrowarrow);
        }
        if (throwarrow != null)
        {
            StopCoroutine(throwarrow);
        }

    }

    public void OnReset()
    {
        m_lineRenderer.gameObject.SetActive(true);
        canShoot = false;
        m_Vision.isinRange = false;
        m_Vision.m_TriggerObject = null;
        if (Ithrowarrow !=null)
        {
            StopCoroutine(Ithrowarrow);
        }
        if (throwarrow != null)
        {
            StopCoroutine(throwarrow);
        }
        
    }

    public void OnGameFail()
    {
        canShoot = false;
        m_Vision.isinRange = false;
        m_Vision.m_TriggerObject = null;
        if (Ithrowarrow != null)
        {
            StopCoroutine(Ithrowarrow);
        }
        if (throwarrow != null)
        {
            StopCoroutine(throwarrow);
        }
    }

    public void ReachingGroundPoint()
    {
        islinerenderer = false;
        m_lineRenderer.gameObject.SetActive(false);
    }

    public void GameStart()
    {
        m_lineRenderer.gameObject.SetActive(true);
    }


    #endregion Game States


}
