using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowThrower : MonoBehaviour
{
    #region Data

    [SerializeField] private GameObject m_ArrowPoint;
    public bool CanShootArrow;
    public float ShootWaitTime;
    #endregion Data

    #region Unity Initialization
    private void Start()
    {

    }

    private void Update()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            if (CanShootArrow)
            {
                StartCoroutine(IArrowThrower());
            }
        }
    }

    private void OnEnable()
    {

    }

    private void OnDisable()
    {

    }
    #endregion Unity Initialization

    #region Game States

    IEnumerator IArrowThrower()
    {
        CanShootArrow = false;
        var arrow = PoolManager.Instance.Dequeue(ePoolType.ArrowStraight);
        arrow.transform.position = this.transform.position;
        GameManager.Instance.Player.OpenDangerObject();
        yield return new WaitForSeconds(1f);
        arrow.SetActive(true);
        var position = GameManager.Instance.Player.ArrowPoint.transform.position;
        ThrowerDelegate.ThrowArrow(position, arrow);
        yield return new WaitForSeconds(ShootWaitTime);
        CanShootArrow = true;
        
    }


    #endregion Game States
}
