using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVision : MonoBehaviour
{
    #region Data
    public GameObject m_TriggerObject;
    public bool isinRange;
    #endregion Data



    #region Trigger

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(nameof(eTagType.Player)))
        {
            m_TriggerObject = other.gameObject;
            isinRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        isinRange = false;
    }

    #endregion Trigger
}
