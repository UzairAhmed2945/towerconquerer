using UnityEngine;

public class ThrowerDelegate
{
    public delegate void OnThrowDelegate(Vector3 i_Target, GameObject i_Arrow);

    public static OnThrowDelegate OnThrowArrow;

    public static void ThrowArrow(Vector3 i_Target, GameObject i_Arrow)
    {
        if(OnThrowArrow != null)
        {
            OnThrowArrow.Invoke(i_Target,i_Arrow);
        }
    }
}
