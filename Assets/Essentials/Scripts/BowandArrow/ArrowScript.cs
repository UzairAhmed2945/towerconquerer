using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowScript : MonoBehaviour
{
    #region Data
    [SerializeField] private ThrowSimulation m_ParentObject;
    [SerializeField] private BoxCollider m_Collider;
    [SerializeField] private TrailRenderer m_trail;
    [SerializeField] private ParticleSystem m_Hitparticle;
    public ePoolType PoolType;

    public float MaxCoolDownTime=4;
    #endregion Data

    private void OnEnable()
    {
        InvokeRepeating(nameof(ForceCloseArrow), MaxCoolDownTime,1);
        m_Collider.enabled = true;
        GameManagerDelegate.OnGameReset += Destroy;

    }

    private void OnDisable()
    {
        CancelInvoke();
        GameManagerDelegate.OnGameReset -= Destroy;
    }

    private void Update()
    {
        //OffScreenPlayerArrowManager.Instance.ShowArrowIfPlayerOffscreen(this.transform, Color.red, true, true);
    }

    #region Trigger
    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(DequeAfterTime());
        ContactPoint contact = collision.contacts[0];
        m_Hitparticle.transform.position = contact.point;
        m_Hitparticle.Play();
    }

    IEnumerator DequeAfterTime()
    {
        m_ParentObject.canMove = false;
        m_ParentObject.canMoveFoward = false;
        m_Collider.enabled = false;
        yield return new WaitForSeconds(1.5f);
        ForceCloseArrow();
    }

    public void ForceCloseArrow()
    {
        m_Collider.enabled = false;
        m_ParentObject.canMove = false;
        m_ParentObject.canMoveFoward = false;
        m_trail.Clear();
        //m_Collider.enabled = true;
        this.transform.localPosition = Vector3.zero;
        this.transform.localRotation = Quaternion.identity;
        m_ParentObject.transform.localPosition = Vector3.zero;
        m_ParentObject.transform.localRotation = Quaternion.identity;
        //OffScreenPlayerArrowManager.Instance.HidePlayerArrow(this.transform);
        PoolManager.Instance.EnQueue(ePoolType.Arrow, m_ParentObject.gameObject);
    }

    public void Destroy()
    {
        m_ParentObject.canMove = false;
        m_Collider.enabled = true;
        this.transform.localPosition = Vector3.zero;
        this.transform.localRotation = Quaternion.identity;
        m_ParentObject.transform.localPosition = Vector3.zero;
        m_ParentObject.transform.localRotation = Quaternion.identity;
        PoolManager.Instance.EnQueue(PoolType, m_ParentObject.gameObject);
    }
    #endregion Trigger
}
