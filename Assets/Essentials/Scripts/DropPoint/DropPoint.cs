using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropPoint : MonoBehaviour
{
    // Start is called before the first frame update
    public ePoolType PoolType;
    public float MaxCoolDownTime = 4;
    public GameObject ChildrenTargetObject;
   

    private void OnEnable()
    {
        changeRotaion();
        InvokeRepeating(nameof(CloseDropPoint), MaxCoolDownTime, 1);
        GameManagerDelegate.OnGameReset += CloseDropPoint;
    }

    private void OnDisable()
    {
        CancelInvoke();
        GameManagerDelegate.OnGameReset -= CloseDropPoint;

    }

    void CloseDropPoint()
    {
        this.transform.localPosition = Vector3.zero;
        this.transform.localRotation = Quaternion.identity;
        PoolManager.Instance.EnQueue(PoolType, this.gameObject);
        
    }

    void changeRotaion()
    {
        if (GameManager.Instance.Player.isonwall)
        {
            ChildrenTargetObject.transform.localEulerAngles = Vector3.zero;
        }
        else
        {
            ChildrenTargetObject.transform.localEulerAngles = new Vector3(90, 0, 0);
        }
    }


}
