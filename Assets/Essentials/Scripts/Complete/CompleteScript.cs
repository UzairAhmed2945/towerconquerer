using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteScript : MonoBehaviour
{
    #region Trigger
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(nameof(eTagType.Player)))
        {
            GameManager.Instance.GameComplete();
        }
    }
    #endregion Trigger
}
