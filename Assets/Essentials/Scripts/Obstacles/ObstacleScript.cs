using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObstacleScript : MonoBehaviour
{
    public float Xpoisition;
    public float speed;
    public Vector3 Initialposition;
    private void Start()
    {
        Initialposition = this.transform.localPosition;
    }

    private void OnEnable()
    {

        GameManagerDelegate.OnGameReset += OnReset;
        TweenObstacle();
        
    }
    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= OnReset;
    }

    void TweenObstacle()
    {
        this.transform.DOLocalMoveX(Xpoisition, speed).SetLoops(-1,LoopType.Yoyo);
    }

    void OnReset()
    {
        DOTween.Kill(this.transform);
        this.transform.localPosition = Initialposition;
        TweenObstacle();
    }
}
