using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCloseScript : MonoBehaviour
{
    #region Data
    [SerializeField] private Collider m_Collider;
    [SerializeField] private MeshRenderer m_Mesh;
    [SerializeField] private ParticleSystem m_Dust;
    [SerializeField] private MeshRenderer wheel1;
    [SerializeField] private MeshRenderer wheel2;

    #endregion Data

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += ResetObstacle;
    }
    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= ResetObstacle;
    }



    private void ResetObstacle()
    {
        m_Collider.enabled = true;
        m_Mesh.enabled = true;
        if (wheel1 != null)
        {
            wheel1.enabled = true;
        }
        if (wheel2 != null)
        {
            wheel2.enabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(nameof(eTagType.Player)))
        {
            m_Dust.Play();
            m_Collider.enabled = false;
            m_Mesh.enabled = false;
            if(wheel1 != null)
            {
                wheel1.enabled = false;
            }
            if (wheel2 != null)
            {
                wheel2.enabled = false;
            }
        }
    }
}
