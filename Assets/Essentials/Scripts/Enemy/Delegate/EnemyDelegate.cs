
public class EnemyDelegate
{
    public delegate void OnEnemyDelegate();
    public static OnEnemyDelegate OnAttack;
    public static OnEnemyDelegate OnChangeEnemy;
    public static OnEnemyDelegate OnChangeTarget;
    public static OnEnemyDelegate onChangeLineRenderer;


    public static void Attack()
    {
        if (OnAttack != null)
        {
            OnAttack.Invoke();
        }
    }
    public static void ChangeEnemy()
    {
        if(OnChangeEnemy != null)
        {
            OnChangeEnemy.Invoke();
        }
    }

    public static void ChangeTarget()
    {
        if(OnChangeTarget != null)
        {
            OnChangeTarget.Invoke();
        }
    }

    public static void ChangeLineRenderer()
    {
        if(onChangeLineRenderer != null)
        {
            onChangeLineRenderer.Invoke();
        }
    }


}
