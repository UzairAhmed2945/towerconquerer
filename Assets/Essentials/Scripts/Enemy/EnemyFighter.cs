using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyFighter : MonoBehaviour
{
    #region Data
    [SerializeField] private EnemyFighterVision m_EnemyVision;
    [SerializeField] private NavMeshAgent m_NavmeshAgent;
    [SerializeField] private Animator m_Animator;
    [SerializeField] private Vector3 m_Position;
    [SerializeField] private Quaternion m_Rotation;
    [SerializeField] private Image HealthFillBar;
    [SerializeField] private GameObject CanvasObject;
    [SerializeField] private Collider m_Collider;
    [SerializeField] private EnemyManager m_EnemyManager;
    public int MaxHp;
    public int CurrentHp;
    public int DecreaseHp;

    #endregion Data
    private void Start()
    {
        m_Position = this.transform.localPosition;
        m_Rotation = this.transform.localRotation;
        CurrentHp = MaxHp;
    }
    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += OnReset;
        GameManagerDelegate.OnGameComplete += oncomplete;
        GameManagerDelegate.OnGameFail += oncomplete;
        EnemyDelegate.OnAttack += OnOpen;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= OnReset;
        GameManagerDelegate.OnGameComplete -= oncomplete;
        EnemyDelegate.OnAttack -= OnOpen;
        GameManagerDelegate.OnGameFail -= oncomplete;
    }


    private void Update()
    {
        if (m_EnemyVision.isplayer)
        {
            var distance = Vector3.Distance(this.gameObject.transform.position, m_EnemyVision.TargetObject.transform.position);
            if (m_NavmeshAgent.stoppingDistance < distance)
            {
                m_NavmeshAgent.enabled = true;
                m_NavmeshAgent.isStopped = false;
                m_NavmeshAgent.SetDestination(m_EnemyVision.TargetObject.transform.position);
                this.gameObject.transform.LookAt(m_EnemyVision.TargetObject.transform);
                m_Animator.SetBool(nameof(eAnimStates.Run), true);
                m_Animator.SetBool(nameof(eAnimStates.Hit), false);

            }
            else
            {
                m_NavmeshAgent.enabled = false;
                m_Animator.SetBool(nameof(eAnimStates.Hit), true);
                m_Animator.SetBool(nameof(eAnimStates.Run), false);


            }
        }
        CanvasObject.transform.LookAt(Camera.main.transform);
    }

    public void OnReset()
    {
        m_Collider.enabled = true;
        this.transform.localPosition = m_Position;
        this.transform.localRotation = m_Rotation;
        m_Animator.SetBool(nameof(eAnimStates.Dead), false);
        m_Animator.SetBool(nameof(eAnimStates.Run), false);
        m_Animator.SetBool(nameof(eAnimStates.Hit), false);
        m_EnemyVision.gameObject.SetActive(false);
        m_NavmeshAgent.enabled = false;
        m_EnemyVision.isplayer = false;
        m_EnemyVision.TargetObject = null;
        CurrentHp = MaxHp;
        HealthFillBar.fillAmount = 1f;

    }

    void oncomplete()
    {
        m_EnemyVision.gameObject.SetActive(false);
        m_NavmeshAgent.enabled = false;
        m_EnemyVision.isplayer = false;
        m_EnemyVision.TargetObject = null;
        //m_Animator.SetBool(nameof(eAnimStates.Dead), false);
        m_Animator.SetBool(nameof(eAnimStates.Run), false);
        m_Animator.SetBool(nameof(eAnimStates.Hit), false);
    }

    void OnOpen()
    {
        m_EnemyVision.gameObject.SetActive(true);
        m_NavmeshAgent.enabled = true;
    }

    void ApplyDamage()
    {
        CurrentHp = CurrentHp - DecreaseHp;
        HealthFillBar.fillAmount = GameManager.Instance.Remaping(CurrentHp,0,MaxHp,0,1);

        if (CurrentHp <= 0)
        {
            m_Collider.enabled = false;
            m_NavmeshAgent.enabled = false;
            m_EnemyVision.gameObject.SetActive(false);
            m_Animator.SetBool(nameof(eAnimStates.Dead), true);
            m_EnemyManager.DecreaseEnemy();
        }

        if (m_EnemyManager.EnemiesCount <=0)
        {
            GameManager.Instance.GameComplete();
            m_NavmeshAgent.enabled = false;
            m_EnemyVision.gameObject.SetActive(false);
            m_Animator.SetBool(nameof(eAnimStates.Dead), true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(nameof(eTagType.DamageEnemy)))
        {
            ApplyDamage();
        }
    }
}
