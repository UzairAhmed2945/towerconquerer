using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFighterVision : MonoBehaviour
{
    #region Data
    public bool isplayer;
    public GameObject TargetObject;
    public eVisionType e_visiontype;
    #endregion Data

    private void OnTriggerStay(Collider other)
    {
        if (eVisionType.Enemy == e_visiontype)
        {
            if (other.CompareTag(nameof(eTagType.Player)))
            {
                isplayer = true;
                TargetObject = other.gameObject;
            }

            else if (other.CompareTag(nameof(eTagType.Follower)))
            {
                isplayer = true;
                TargetObject = other.gameObject;
            }


        }

        else if(eVisionType.player == e_visiontype)
        {
            if (other.CompareTag(nameof(eTagType.Enemy)))
            {
                isplayer = true;
                TargetObject = other.gameObject;
            }
        }
    }
}
