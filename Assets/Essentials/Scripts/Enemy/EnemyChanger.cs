using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChanger : MonoBehaviour
{
    public EnemyFighter FighterObject;
    public EnemyThrower ArrowThrowerObject;
    public GameObject Bat;
    public GameObject Bow;
    public Collider Fightercollider;


    private void Start()
    {
        
    }

    private void OnEnable()
    {
        //GameManagerDelegate.OnGameNext += GameNext;
        EnemyDelegate.OnChangeEnemy += GameNext;
        GameManagerDelegate.OnGameReset += Gamereset;
        StartCoroutine(closecollider());
    }

    private void OnDisable()
    {
        //GameManagerDelegate.OnGameNext -= GameNext;
        EnemyDelegate.OnChangeEnemy -= GameNext;
        GameManagerDelegate.OnGameReset -= Gamereset;
    }

    void GameNext()
    {
        Bat.SetActive(true);
        Bow.SetActive(false);
        ArrowThrowerObject.enabled = false;
        FighterObject.enabled = true;
        Fightercollider.enabled = true;
        FighterObject.OnReset();
    }

    private void Gamereset()
    {
        Bat.SetActive(false);
        Bow.SetActive(true);
        FighterObject.OnReset();
        FighterObject.enabled = false;
        ArrowThrowerObject.enabled =true;
        ArrowThrowerObject.OnReset();
        Fightercollider.enabled = false;

    }

    IEnumerator closecollider()
    {
        yield return new WaitForSeconds(1f);
        Fightercollider.enabled = false;

    }
}
