using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public int LevelEnemiesCount;
    public int EnemiesCount;

    private void Start()
    {
        EnemiesCount = LevelEnemiesCount;
    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += onReset;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= onReset;
    }

    void onReset()
    {
        EnemiesCount = LevelEnemiesCount;
    }

    public void DecreaseEnemy()
    {
        EnemiesCount = EnemiesCount - 1;
    }
}
