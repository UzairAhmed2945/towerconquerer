using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteConfetti : MonoBehaviour
{
    public GameObject [] ConfettiParticles;

    private void OnEnable()
    {
        GameManagerDelegate.OnGameComplete += OnComplete;
        GameManagerDelegate.OnGameReset += OnReset;

    }
    private void OnDisable()
    {
        GameManagerDelegate.OnGameComplete -= OnComplete;
        GameManagerDelegate.OnGameReset -= OnReset;
    }


    void OnComplete()
    {
        for (int i = 0; i < ConfettiParticles.Length; i++)
        {
            ConfettiParticles[i].SetActive(true);
        }
    }

    void OnReset()
    {
        for (int i = 0; i < ConfettiParticles.Length; i++)
        {
            ConfettiParticles[i].SetActive(false);
        }
    }
       


}
