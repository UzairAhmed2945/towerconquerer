using Facebook.Unity;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    #region Data
    [Space]
    public eGameStates e_gamestates = eGameStates.Idle;
    public PlayerScript Player;
    #endregion Data


    public void Gamestart()
    {
        GameManager.Instance.e_gamestates = eGameStates.Playing;
        GameManagerDelegate.GameStart();
        if (StorageManager.Instance.Vibration == 0)
        {
            MMVibrationManager.Haptic(HapticTypes.SoftImpact);
        }

        if (FB.IsInitialized)
        {
            FB.LogAppEvent("Level Started", StorageManager.Instance.CurrentlevelNo + 1);
        }
    }

    public void GameFail()
    {
        GameManager.Instance.e_gamestates = eGameStates.Fail;
        GameManagerDelegate.GameFail();
        if (StorageManager.Instance.Vibration == 0)
        {
            MMVibrationManager.Haptic(HapticTypes.Failure);
        }

        if (FB.IsInitialized)
        {
            FB.LogAppEvent("Level Failed", StorageManager.Instance.CurrentlevelNo + 1);
        }
    }

    public void GameReset()
    {
        GameManager.Instance.e_gamestates = eGameStates.Idle;
        GameManagerDelegate.GameReset();

        if (FB.IsInitialized)
        {
            FB.LogAppEvent("Level Reset", StorageManager.Instance.CurrentlevelNo + 1);
        }
    }

    public void GameComplete()
    {
        GameManager.Instance.e_gamestates = eGameStates.Complete;
        StorageManager.Instance.CurrentlevelNo++;
        GameManagerDelegate.GameComplete();
        if (StorageManager.Instance.Vibration == 0)
        {
            MMVibrationManager.Haptic(HapticTypes.Success);
        }

        if (FB.IsInitialized)
        {
            FB.LogAppEvent("Level Complete", StorageManager.Instance.CurrentlevelNo + 1);
        }
    }

    public void GameNext()
    {
        GameManager.Instance.e_gamestates = eGameStates.GameNext;
        GameManagerDelegate.GameNext();
    }

    public float Remaping(float i_From, float i_FromMin, float i_FromMax, float i_ToMin, float i_ToMax)
    {
        float fromAbs = i_From - i_FromMin;
        float fromMaxAbs = i_FromMax - i_FromMin;

        float normal = fromAbs / fromMaxAbs;

        float toMaxAbs = i_ToMax - i_ToMin;
        float toAbs = toMaxAbs * normal;

        float to = toAbs + i_ToMin;

        return to;
    }
}

