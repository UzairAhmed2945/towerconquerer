﻿
public class GameManagerDelegate
{
    public delegate void OnGameManagerDelegate();
    public static OnGameManagerDelegate OnGameFail;
    public static OnGameManagerDelegate OnGameStart;
    public static OnGameManagerDelegate OnGameReset;
    public static OnGameManagerDelegate OnGameComplete;
    public static OnGameManagerDelegate OnGameNext;


    public static void GameFail()
    {
        if (OnGameFail != null)
        {
            OnGameFail.Invoke();
        }
    }
    public static void GameStart()
    {
        if (OnGameStart != null)
        {
            OnGameStart.Invoke();
        }
    }

    public static void GameReset()
    {
        if (OnGameReset != null)
        {
            OnGameReset.Invoke();
        }
    }

    public static void GameComplete()
    {
        if(OnGameComplete != null)
        {
            OnGameComplete.Invoke();
        }
    }

    public static void GameNext()
    {
        if (OnGameNext != null)
        {
            OnGameNext.Invoke();
        }
    }

}
