using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [SerializeField] public GameObject CamMoveTarget;
    [SerializeField] public Transform LookAtTarget;
    private Transform m_Player;
    private Quaternion m_InitialRotaion;
    private Vector3 m_InitialPosition;
    [SerializeField] private Transform m_Cam;
    [SerializeField] private bool m_CanMove;
    [SerializeField] private Vector3 m_TargetOffset;
    [SerializeField] private Vector3 m_LookAtHeight;
    [SerializeField] private float m_Speed;
    [SerializeField] private float m_sideSpeed;


    #region Unity Initialization


    private void Awake()
    {
        m_InitialPosition = this.transform.position;
       // setInitialTargets();
    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameStart += onLevelStarted;
        GameManagerDelegate.OnGameReset += onReset;
    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameStart -= onLevelStarted;
        GameManagerDelegate.OnGameReset -= onReset;
    }

    #endregion Unity Initialization

    #region Unity Loops

    void LateUpdate()
    {
        if (m_CanMove)
        {
            float interpolation = m_Speed * Time.deltaTime;

            //Vector3 position = this.transform.position;
            //position.z = Mathf.Lerp(this.transform.position.z, LookAtTarget.transform.position.z + m_TargetOffset.y, interpolation);
            //position.x = Mathf.Lerp(this.transform.position.x, LookAtTarget.transform.position.x + m_TargetOffset.x, interpolation);
            //position.y = Mathf.Lerp(this.transform.position.y, CamMoveTarget.transform.position.y + m_TargetOffset.z, interpolation);

            //this.transform.position = position;

            transform.position = Vector3.Lerp(transform.position, LookAtTarget.transform.position + m_TargetOffset, interpolation);

            //m_LookAtHeight.z = GameManager.Instance.Remaping(m_Player.transform.localPosition.x, -GameConfig.Instance.Player.SideDistance, GameConfig.Instance.Player.SideDistance, -0.2f, 0.2f);
           // m_Cam.transform.LookAt(LookAtTarget.position + m_LookAtHeight);

            //m_Cam.transform.localEulerAngles = new Vector3(m_LookAtHeight.y, m_Cam.transform.localEulerAngles.y, 0);
        }
        else
        {
            float interpolation = m_Speed * Time.deltaTime;
            float sidespeed = m_sideSpeed * Time.deltaTime;
            //this.transform.position = CamMoveTarget.transform.position;
            var CameraPointPos = CamMoveTarget.transform.localPosition;
            CameraPointPos.x = Mathf.Lerp(CameraPointPos.x, LookAtTarget.transform.localPosition.x, sidespeed);
            CamMoveTarget.transform.localPosition = CameraPointPos;


            transform.position = Vector3.Lerp(transform.position, CamMoveTarget.transform.position, interpolation);
            m_Cam.transform.localEulerAngles = new Vector3(m_LookAtHeight.y, m_Cam.transform.localEulerAngles.y, 0);

           

        }
    }

    #endregion Unity Loops

    void setInitialTargets()
    {
        onReset();
        onLevelStarted();
    }

    void onLevelStarted()
    {
        //m_CanMove = true;
        m_InitialRotaion = this.transform.rotation;

    }


    void onReset()
    {

        m_CanMove = false;

        Vector3 position = this.transform.position;
        position.z = CamMoveTarget.transform.position.z + m_TargetOffset.y;// Mathf.Lerp(this.transform.position.z, CamMoveTarget.transform.position.z + m_TargetOffset.y, interpolation);
        position.x = CamMoveTarget.transform.position.x + m_TargetOffset.x;//Mathf.Lerp(this.transform.position.x, CamMoveTarget.transform.position.x + m_TargetOffset.x, interpolation);
        position.y = CamMoveTarget.transform.position.y + m_TargetOffset.z;//Mathf.Lerp(this.transform.position.y, CamMoveTarget.transform.position.y + m_TargetOffset.z, interpolation);

        this.transform.position = position;
        this.transform.rotation = m_InitialRotaion;
        this.transform.position = m_InitialPosition;
        //this.transform.position = CamMoveTarget.transform.position;

        //m_Cam.transform.localEulerAngles = Vector3.zero;

        //m_LookAtHeight.z = GameManager.Instance.Remaping(m_Player.transform.localPosition.x, -GameConfig.Instance.Player.SideDistance, GameConfig.Instance.Player.SideDistance, -0.2f, 0.2f);
        //m_Cam.transform.LookAt(LookAtTarget.position + m_LookAtHeight);

        m_Cam.transform.localEulerAngles = new Vector3(m_LookAtHeight.y, m_Cam.transform.localEulerAngles.y, 0);

    }
}
