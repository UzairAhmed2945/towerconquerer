public enum eTagType {
    Player,
    Arrow,
    ArrowVision,
    Complete,
    GroundPoint,
    UpperPoint,
    Enemy,
    DamagePlayer,
    DamageEnemy,
    Obstacle,
    Follower,
    Coin,
    CompleteGround,
    Dead

}


