using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;
using UnityEngine.AI;
using MoreMountains.NiceVibrations;
using DG.Tweening;

public class PlayerScript : MonoBehaviour
{
    #region Data
    [Header("Player Component")]
    [SerializeField] private Collider m_Collider;
    [SerializeField] private GameObject m_PlayerObject;
    [SerializeField] private GameObject m_Player;
    [SerializeField] private Rigidbody m_PlayerRigidBody;
    [SerializeField] private float m_PlayerXOffset;
    [SerializeField] private float m_SideSpeed;
    [SerializeField] private float m_PlayerSideLerpSpeed;
    [SerializeField] private Vector3 m_PlayerInitialPosition;
    [SerializeField] private Vector3 m_PlayerObjectInitialPosition;
    private bool canplayerturn;
    [Header("Enemy Arrow")]
    [SerializeField] private float m_ArrowpointSideSpeed;
    public int DecreaseFollowerCount;
    public GameObject ArrowPoint;
    public GameObject DangerAnimation;
    [SerializeField] private Vector3 m_ArrowpointInitialPosition;
    [SerializeField] private Vector3 m_ArrowpointClimbPosition;
    [SerializeField] private ArrowThrower m_ArrowThrower;
    public GameObject redLinePoint;

    [Header("Followers")]
    [Space]
    public List<FollowerScript> Followers;
    public ParticleSystem puffParticle;

    [Header("Spline Component")]
    [SerializeField] private SplineFollower m_SplineFollower;
    [SerializeField] private SplineComputer m_SPlineCom1;
    [SerializeField] private float m_PlayerForwardSpeed;
    [SerializeField] private float m_PlayerClimbSpeed;

    [Header("Player Animation")]
    [Space]
    [SerializeField] private Animator m_PlayerAnimator;

    [Header("Nav MEsh")]
    [Space]
    [SerializeField] private NavMeshAgent m_NavmeshAgent;
    public Animator m_DoorAnimator;
    [SerializeField] private EnemyFighterVision m_EnemyVision;
    public int MaxHp;
    public int CurrentHp;
    public int DecreaseHp;
    public GameObject Bat;
    public GameObject Shield;
    public bool isGameFail;
    public bool isonwall;

    #endregion Data
    #region Unity Initialization

    private void OnEnable()
    {
        GameManagerDelegate.OnGameStart += GameStart;
        GameManagerDelegate.OnGameFail += GameFail;
        GameManagerDelegate.OnGameComplete += GameComplete;
        GameManagerDelegate.OnGameNext += GameNext;
        CurrentHp = MaxHp;


    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameStart -= GameStart;
        GameManagerDelegate.OnGameFail -= GameFail;
        GameManagerDelegate.OnGameComplete -= GameComplete;
        GameManagerDelegate.OnGameNext -= GameNext;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            if (InputManager.Instance.EInputType == eInputType.Control1)
            {
                var position = m_PlayerObject.transform.localPosition;
                position.x += InputManager.Instance.DeltaDrag.x * m_SideSpeed * Time.deltaTime;
                position.x = Mathf.Clamp(position.x, -m_PlayerXOffset, m_PlayerXOffset);
                m_PlayerObject.transform.localPosition = position;

                var PlayerPos = m_Player.transform.localPosition;
                PlayerPos.x = Mathf.Lerp(PlayerPos.x, m_PlayerObject.transform.localPosition.x, m_PlayerSideLerpSpeed * Time.deltaTime);
                m_Player.transform.localPosition = PlayerPos;

                if (!canplayerturn)
                {
                    m_Player.transform.LookAt(m_PlayerObject.transform);
                    
                }


                //var position = m_Player.transform.position;
                //position.x += InputManager.Instance.DeltaDrag.x * m_SideSpeed * Time.deltaTime;
                //position.x = Mathf.Clamp(position.x, -m_PlayerXOffset, m_PlayerXOffset);
                //m_Player.transform.position = position;

                //m_Player.transform.localEulerAngles = new Vector3(m_Player.transform.localEulerAngles.x, m_Player.transform.localEulerAngles.y + InputManager.Instance.DeltaDrag.x *8 , m_Player.transform.localEulerAngles.z);





                var ArrowPos = ArrowPoint.transform.localPosition;
                ArrowPos.x = Mathf.Lerp(ArrowPos.x, m_Player.transform.localPosition.x, m_ArrowpointSideSpeed * Time.deltaTime);
                ArrowPoint.transform.localPosition = ArrowPos;

                DangerAnimation.transform.LookAt(Camera.main.transform);

            }
        }

        //m_Player.transform.localRotation = Quaternion.Lerp(m_Player.transform.localRotation, Quaternion.identity, 0.1f);


        if (GameManager.Instance.e_gamestates == eGameStates.GameNext)
        {
            var PlayerObjectPos = m_PlayerObject.transform.localPosition;
            PlayerObjectPos.x = Mathf.MoveTowards(PlayerObjectPos.x, m_SplineFollower.gameObject.transform.position.x, m_SideSpeed * Time.deltaTime);
            m_PlayerObject.transform.localPosition = PlayerObjectPos;

            var PlayerPos = m_Player.transform.localPosition;
            PlayerPos.x = Mathf.MoveTowards(PlayerPos.x, m_PlayerObject.transform.localPosition.x, m_PlayerSideLerpSpeed * Time.deltaTime);
            m_Player.transform.localPosition = PlayerPos;
        }

        if (m_EnemyVision.isplayer)
        {
            if (!isGameFail)
            {
                var distance = Vector3.Distance(this.gameObject.transform.position, m_EnemyVision.TargetObject.transform.position);
                if (m_NavmeshAgent.stoppingDistance < distance)
                {
                    m_NavmeshAgent.enabled = true;
                    m_NavmeshAgent.SetDestination(m_EnemyVision.TargetObject.transform.position);
                    this.gameObject.transform.LookAt(m_EnemyVision.TargetObject.transform);
                    m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), true);
                    m_PlayerAnimator.SetBool(nameof(eAnimStates.Hit), false);

                }
                else
                {
                    m_NavmeshAgent.enabled = false;
                    m_PlayerAnimator.SetBool(nameof(eAnimStates.Hit), true);
                    m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);


                }
            }
        }



    }
    #endregion Unity Initialization



    #region Game States
    private void SetPlayerSpline(SplineComputer i_SplineComputer)
    {
        m_SplineFollower.spline = i_SplineComputer;
    }

    public void SetPlayer(SplineComputer i_splineComputer)
    {
        m_SPlineCom1 = i_splineComputer;
        SetPlayerSpline(i_splineComputer);
        ResetPlayerValues();
    }
    void ResetPlayerValues()
    {
        m_Player.gameObject.tag = nameof(eTagType.Player);
        m_Collider.enabled = true;
        m_SplineFollower.follow = false;
        m_Player.transform.localPosition = m_PlayerInitialPosition;
        m_PlayerObject.transform.localPosition = m_PlayerObjectInitialPosition;
        m_Player.transform.localRotation = Quaternion.identity;
        m_SplineFollower.Restart(0);
        m_SplineFollower.RebuildImmediate();
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Dead), false);
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Climb), false);
        m_PlayerAnimator.ResetTrigger(nameof(eAnimStates.ClimbUp));
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Hit), false);
        m_DoorAnimator.ResetTrigger(nameof(eAnimStates.Door));
        m_DoorAnimator.Rebind();
        Followers.Clear();
        ArrowPoint.transform.localPosition = m_ArrowpointInitialPosition;
        m_EnemyVision.gameObject.SetActive(false);
        m_NavmeshAgent.enabled = false;
        m_EnemyVision.isplayer = false;
        m_EnemyVision.TargetObject = null;
        CurrentHp = MaxHp;
        Bat.SetActive(false);
        Shield.SetActive(false);
        canplayerturn = false;
        m_PlayerRigidBody.useGravity = false;
        m_PlayerRigidBody.isKinematic = true;
        isGameFail = false;
        isonwall = false;
    }

    void GameStart()
    {
        m_SplineFollower.followSpeed = m_PlayerForwardSpeed;
        m_SplineFollower.follow = true;
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), true);

    }

    void GameFail()
    {
        isGameFail = true;
        m_SplineFollower.follow = false;
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Dead), true);
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);

    }

    void GameComplete()
    {
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);
        m_PlayerAnimator.ResetTrigger(nameof(eAnimStates.ClimbUp));
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Hit), false);
        m_SplineFollower.follow = false;
        m_EnemyVision.gameObject.SetActive(false);
        m_NavmeshAgent.enabled = false;
        m_EnemyVision.isplayer = false;
        m_EnemyVision.TargetObject = null;
    }

    void GameNext()
    {
        EnemyDelegate.ChangeEnemy();
        //m_ArrowThrower.CanShootArrow = false;
    }

    void OnGroundComplete()
    {
        m_DoorAnimator.SetTrigger(nameof(eAnimStates.Door));
    }
    public void OpenDangerObject()
    {
        StartCoroutine(DangerObjectCoroutine());
    }

    public IEnumerator DangerObjectCoroutine()
    {
        DangerAnimation.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        DangerAnimation.SetActive(false);
    }

    void ReachingGroundPoint()
    {
        StartCoroutine(IReachingGroundPoint());
    }
    IEnumerator IReachingGroundPoint()
    {
        EnemyDelegate.ChangeLineRenderer();
        isonwall = true;
        yield return new WaitForSeconds(0f);
        canplayerturn = true;
        m_Player.transform.localEulerAngles = Vector3.zero;
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Climb), true);
        m_SplineFollower.followSpeed = m_PlayerClimbSpeed;
        GameManager.Instance.e_gamestates = eGameStates.Playing;
        ArrowPoint.transform.localPosition = m_ArrowpointClimbPosition;
        for (int i = 0; i < Followers.Count; i++)
        {
            //Followers[i].gameObject.SetActive(false);
            //Followers[i].isfollowing = false;
            Followers[i].islookat = false;
            var rotation = Followers[i].transform.rotation;
            rotation.y = 0f;
            Followers[i].transform.rotation = rotation;
            //var randonnox = Random.Range(-5, 5);
            //var randonnoy = 3;
            //Followers[i].transform.position = new Vector3(m_Player.transform.position.x + randonnox, m_Player.transform.position.y +randonnoy, m_Player.transform.position.z);



        }
        yield return new WaitForSeconds(0.25f);
        if (Followers.Count > 0)
        {
            // puffParticle.Play();
        }
        yield return new WaitForSeconds(0.25f);
        for (int i = 0; i < Followers.Count; i++)
        {
           // Followers[i].ParentObject.transform.localEulerAngles = Followers[i].AnimationRotation;
            Followers[i].m_FollowerAgent.speed = 10;
            Followers[i].gameObject.SetActive(true);
            Followers[i].m_Ainmator.SetBool(nameof(eAnimStates.Run), false);
            Followers[i].m_Ainmator.SetBool(nameof(eAnimStates.Climb), true);


        }
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < Followers.Count; i++)
        {
            Followers[i].ParentObject.transform.DOLocalRotate(Followers[i].AnimationRotation, 0.6f);
            Followers[i].m_FollowerAgent.radius = 1.3f;
        }
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < Followers.Count; i++)
        {
            Followers[i].m_FollowerAgent.radius = 1.3f;
        }



    }

    void ReachingUpPoint()
    {

        StartCoroutine(IReachingPoint());
    }
     
    IEnumerator IReachingPoint()
    {
        
        m_PlayerAnimator.SetTrigger(nameof(eAnimStates.ClimbUp));
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), true);
        yield return new WaitForSeconds(0.3f);
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Climb), false);
        yield return new WaitForSeconds(0.2f);
        
        for (int i = 0; i < Followers.Count; i++)
        {
            //Followers[i].gameObject.SetActive(false);
           
        }
        yield return new WaitForSeconds(0f);
        for (int i = 0; i < Followers.Count; i++)
        {

           // Followers[i].isfollowing= false;
            Followers[i].m_Ainmator.SetTrigger(nameof(eAnimStates.ClimbUp));
            Followers[i].m_Ainmator.SetBool(nameof(eAnimStates.Climb), false);
            var rotation = Followers[i].transform.rotation;
            rotation.y = 0f;
            Followers[i].transform.rotation = rotation;
            Followers[i].m_FollowerAgent.radius = 0.8f;
            
        }
        yield return new WaitForSeconds(1f);

        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);

        if (Followers.Count > 0)
        {
            //puffParticle.Play();
        }
        yield return new WaitForSeconds(0f);
        for (int i = 0; i < Followers.Count; i++)
        {
           
            Followers[i].ParentObject.transform.localEulerAngles = Vector3.zero;
            Followers[i].m_FollowerAgent.stoppingDistance = 4;
            Followers[i].isfollowing = true;
            //var randonnox = Random.Range(-3, 3);
            //var randonnoz = Random.Range(-3, 3);
            //Followers[i].transform.position = new Vector3(m_Player.transform.position.x + randonnox, m_Player.transform.position.y, m_Player.transform.position.z + randonnoz);
            //Followers[i].gameObject.SetActive(true);
        }

        yield return new WaitForSeconds(1f);
        m_SplineFollower.follow = false;
        
        
       yield return new WaitForSeconds(0.5f);
        if (!isGameFail)
        {
            Bat.SetActive(true);
            Shield.SetActive(true);
            m_EnemyVision.gameObject.SetActive(true);
            m_NavmeshAgent.enabled = true;
            m_NavmeshAgent.isStopped = false;
            EnemyDelegate.Attack();
        }
        for (int i = 0; i < Followers.Count; i++)
        {
            Followers[i].m_Collider.enabled = true;
            Followers[i].m_EnemyVision.gameObject.SetActive(true);
            Followers[i].Bat.SetActive(true);
            Followers[i].isfollowing = false;
            Followers[i].islookat = false;
        }
    }

    void ApplyDamage()
    {
        if (!isGameFail)
        {
            CurrentHp = CurrentHp - DecreaseHp;
            if (CurrentHp <= 0)
            {
                m_Player.gameObject.tag = nameof(eTagType.Dead);
                m_PlayerAnimator.SetBool(nameof(eAnimStates.Dead), true);
                m_NavmeshAgent.enabled = false;
                m_EnemyVision.gameObject.SetActive(false);
                m_PlayerAnimator.SetBool(nameof(eAnimStates.Hit), false);
                m_Collider.enabled = false;
                Bat.SetActive(false);
                Shield.SetActive(false);
            }

            if (CurrentHp <= 0 && Followers.Count <= 0)
            {

                GameManager.Instance.GameFail();

            }
        }


    }

    

    #endregion Game States


    #region Trigger

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(nameof(eTagType.Arrow)))
        {
            if (!isGameFail)
            {
                if (Followers.Count >= 0)
                {
                    if (DecreaseFollowerCount <= Followers.Count )
                    {
                        for (int i = 0; i < DecreaseFollowerCount; i++)
                        {
                            var Followerdead = PoolManager.Instance.Dequeue(ePoolType.FollowerDead);
                            Followerdead.transform.position = this.transform.position;
                            Followers[0].gameObject.SetActive(false);
                            Followerdead.SetActive(true);
                            Followers.RemoveAt(0);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < Followers.Count; i++)
                        {
                            var Followerdead = PoolManager.Instance.Dequeue(ePoolType.FollowerDead);
                            Followerdead.transform.position = this.transform.position;
                            Followers[0].gameObject.SetActive(false);
                            Followerdead.SetActive(true);
                            Followers.RemoveAt(0);
                        }
                        m_PlayerRigidBody.useGravity = true;
                        m_PlayerRigidBody.isKinematic = false;
                        GameManager.Instance.GameFail();
                        if(Followers.Count > 0)
                        {
                            for (int i = 0; i < Followers.Count; i++)
                            {
                                var Followerdead = PoolManager.Instance.Dequeue(ePoolType.FollowerDead);
                                Followerdead.transform.position = this.transform.position;
                                Followers[0].gameObject.SetActive(false);
                                Followerdead.SetActive(true);
                                Followers.RemoveAt(0);
                            }
                        }

                    }
                    if (StorageManager.Instance.Vibration == 0)
                    {
                        MMVibrationManager.Haptic(HapticTypes.RigidImpact);
                    }
                }
                else
                {
                    m_PlayerRigidBody.useGravity = true;
                    m_PlayerRigidBody.isKinematic = false;
                    GameManager.Instance.GameFail();
                    
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(nameof(eTagType.Complete)))
        {
            GameManager.Instance.GameNext();
        }

        if (other.gameObject.CompareTag(nameof(eTagType.CompleteGround)))
        {
            OnGroundComplete();
        }

        if (other.gameObject.CompareTag(nameof(eTagType.GroundPoint)))
        {
            ReachingGroundPoint();
        }

        if (other.gameObject.CompareTag(nameof(eTagType.UpperPoint)))
        {
            ReachingUpPoint();
        }

        if (other.CompareTag(nameof(eTagType.DamagePlayer)))
        {
            ApplyDamage();
        }

        if (other.CompareTag(nameof(eTagType.Obstacle)))
        {
            if (Followers.Count > 0)
            {
                var Followerdead = PoolManager.Instance.Dequeue(ePoolType.FollowerDead);
                Followerdead.transform.position = this.transform.position;
                Followers[0].gameObject.SetActive(false);
                Followerdead.SetActive(true);
                Followers.RemoveAt(0);
                if (StorageManager.Instance.Vibration == 0)
                {
                    MMVibrationManager.Haptic(HapticTypes.RigidImpact);
                }
            }
            else
            {
                m_PlayerRigidBody.useGravity = true;
                m_PlayerRigidBody.isKinematic = false;
                GameManager.Instance.GameFail();
            }
        }

    }


    #endregion Trigger
}
