using System.Collections;
using System.Collections.Generic;
using Dreamteck.Splines;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Animator m_PlayerAnimator;
    [SerializeField] private Transform m_Character;
    [SerializeField] private float m_SideSpeed;
    [SerializeField] private float m_PlayerXOffset;

    [Space]
    [Header("Spline Component")]
    [SerializeField] private SplineFollower m_SplineFollower;
    [SerializeField] private float m_PlayerForwardSpeed;
    [SerializeField] private float m_PlayerClimbSpeed;

    private void OnEnable()
    {
        GameManagerDelegate.OnGameStart += GameStart;
        GameManagerDelegate.OnGameFail += GameFail;
        GameManagerDelegate.OnGameComplete += GameComplete;


    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameStart -= GameStart;
        GameManagerDelegate.OnGameFail -= GameFail;
        GameManagerDelegate.OnGameComplete -= GameComplete;
    }


    private void FixedUpdate()
    {
        if (GameManager.Instance.e_gamestates == eGameStates.Playing)
        {
            if (InputManager.Instance.EInputType == eInputType.Control1)
            {
                var position = m_Character.transform.position;
                position.x += InputManager.Instance.DeltaDrag.x * m_SideSpeed * Time.deltaTime;
                position.x = Mathf.Clamp(position.x, -m_PlayerXOffset, m_PlayerXOffset);
                m_Character.transform.position = position;

                m_Character.transform.localEulerAngles = new Vector3(m_Character.transform.localEulerAngles.x, m_Character.transform.localEulerAngles.y + InputManager.Instance.DeltaDrag.x * 10, m_Character.transform.localEulerAngles.z);
            }
        }
        m_Character.transform.localRotation = Quaternion.Lerp(m_Character.transform.localRotation, Quaternion.identity, 0.1f);
    }

    void GameStart()
    {
        m_SplineFollower.followSpeed = m_PlayerForwardSpeed;
        m_SplineFollower.follow = true;
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), true);

    }

    void GameFail()
    {
        m_SplineFollower.follow = false;
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Dead), true);
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);

    }

    void GameComplete()
    {
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Run), false);
        m_PlayerAnimator.ResetTrigger(nameof(eAnimStates.ClimbUp));
        m_PlayerAnimator.SetBool(nameof(eAnimStates.Hit), false);
        m_SplineFollower.follow = false;
    }
}
