﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ScreenData
{
     private Vector2 OriginalRes;
     private float OriginalDPI;

     private Vector2 FinalRes;
     public float FinalDPI;
     private Vector2 ScreenSizeInch;
     private Vector2 ScreenSizeCm;

     private Vector2 ScalingVector;
     public float Scaling;


    public void CalculateData(bool i_LogData = false)
    {
        OriginalRes = new Vector2(Screen.width, Screen.height);
        OriginalDPI = Screen.dpi;

#if UNITY_EDITOR
        ScalingVector = GameViewEditorRes.GetGameViewScale();
#else
        ScalingVector = Vector3.one;
#endif
        Scaling = (ScalingVector.x + ScalingVector.y) * .5f;

        FinalRes = new Vector2(Screen.width * ScalingVector.x, Screen.height * ScalingVector.y);

        if(Screen.dpi == 0)
        {
            FinalDPI = 320;
        }
        else
        {
            FinalDPI = Screen.dpi * Scaling;
        }

        ScreenSizeInch = new Vector2((FinalRes.x / FinalDPI) * ScalingVector.x, (FinalRes.y / FinalDPI) * ScalingVector.y);
        ScreenSizeCm = ScreenSizeInch * 2.54f;



        if (!i_LogData)
            return;

        Debug.LogError("Original Resolution - " + OriginalRes);
        Debug.LogError("Original DPI - " + OriginalDPI);
        Debug.LogError("Resolution - " + FinalRes);
        Debug.LogError("DPI - " + FinalDPI);
        Debug.LogError("Size Inches - " + ScreenSizeInch);
        Debug.LogError("Size Cm - " + ScreenSizeCm);

        Debug.LogError("Scaling Vector - " + ScalingVector);
        Debug.LogError("Scaling - " + Scaling);
    }
}
