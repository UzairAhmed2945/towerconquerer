﻿public class InputDelegate 
{
    public delegate void OnInputDelegate(eDirection direction);
    public static OnInputDelegate OnFingerSwipe;


    public static void Swipe_Direction(eDirection i_Direction)
    {
        if (OnFingerSwipe != null)
        {
            OnFingerSwipe.Invoke(i_Direction);
        }
    }
}
