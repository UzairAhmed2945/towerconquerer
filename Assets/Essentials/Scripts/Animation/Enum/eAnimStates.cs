public enum eAnimStates
{
    Run,
    Idle,
    Throw,
    Dead,
    Door,
    Climb,
    ClimbUp,
    Hit
}
