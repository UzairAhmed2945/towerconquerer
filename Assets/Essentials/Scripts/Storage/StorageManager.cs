using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageManager : Singleton<StorageManager>
{
    #region Data
    public int CurrentlevelNo { get { return PlayerPrefs.GetInt(nameof(CurrentlevelNo)); } set { PlayerPrefs.SetInt((nameof(CurrentlevelNo)), value);}}
    public int Vibration { get { return PlayerPrefs.GetInt(nameof(Vibration),0); } set { PlayerPrefs.SetInt((nameof(Vibration)), value); } }
    public int LockNumber { get { return PlayerPrefs.GetInt(nameof(Vibration), 0); } set { PlayerPrefs.SetInt((nameof(Vibration)), value); } }


    #endregion Data
}
