using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowerScript : MonoBehaviour
{
    #region Data

    public NavMeshAgent m_FollowerAgent;
    public Animator m_Ainmator;
    [SerializeField] public Material m_NormalMaterial;
    [SerializeField] public Material M_FollowMaterial;
    [SerializeField] public SkinnedMeshRenderer m_SkinMeshMaterial;
    public Vector3 AnimationRotation;
    public GameObject ParentObject;
    public bool isfollowing;
    public bool islookat;
    public Vector3 m_InitialPosition;
    public Quaternion m_InitialRotation;
    public Collider m_Collider;
    [SerializeField] public EnemyFighterVision m_EnemyVision;
    public int MaxHp;
    public int CurrentHp;
    public int DecreaseHp;
    public GameObject Bat;

    #endregion Data

    #region Unity Initialization
    private void Start()
    {
        m_InitialPosition = this.transform.localPosition;
        m_InitialRotation = this.transform.localRotation;
        CurrentHp = MaxHp;
    }

    private void Update()
    {
        if (isfollowing)
        {
            m_FollowerAgent.SetDestination(GameManager.Instance.Player.gameObject.transform.position);

        }
        if (islookat)
        {
            // this.transform.LookAt(GameManager.Instance.Player.transform.position);

            var lookPos = GameManager.Instance.Player.transform.position - transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 12);
        }


        if (m_EnemyVision.isplayer)
        {
            var distance = Vector3.Distance(this.gameObject.transform.position, m_EnemyVision.TargetObject.transform.position);
            if (m_FollowerAgent.stoppingDistance < distance)
            {
                m_FollowerAgent.enabled = true;
                m_FollowerAgent.isStopped = false;
                m_FollowerAgent.SetDestination(m_EnemyVision.TargetObject.transform.position);
                this.gameObject.transform.LookAt(m_EnemyVision.TargetObject.transform);
                m_Ainmator.SetBool(nameof(eAnimStates.Run), true);
                m_Ainmator.SetBool(nameof(eAnimStates.Hit), false);

            }
            else
            {
                m_FollowerAgent.enabled = false;
                m_Ainmator.SetBool(nameof(eAnimStates.Hit), true);
                m_Ainmator.SetBool(nameof(eAnimStates.Run), false);


            }
        }




    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameComplete += GameComplete;
        GameManagerDelegate.OnGameReset += GameReset;
    }
    private void OnDisable()
    {
        GameManagerDelegate.OnGameComplete -= GameComplete;
        GameManagerDelegate.OnGameReset -= GameReset;
    }
    #endregion Unity Initialization

    #region Game States
    public void PlayerDetected()
    {
        m_FollowerAgent.enabled = true;
        isfollowing = true;
        islookat = true;
        m_SkinMeshMaterial.material = M_FollowMaterial;
        m_Ainmator.SetBool(nameof(eAnimStates.Run),true);
    }

    public void GameReset()
    {
        m_Collider.enabled = false;
        m_FollowerAgent.enabled = false;
        m_FollowerAgent.speed = 30;
        m_FollowerAgent.acceleration = 100;
        m_FollowerAgent.radius = 0.8f;
        ParentObject.transform.localEulerAngles = Vector3.zero;
        isfollowing = false;
        islookat = false;
        m_SkinMeshMaterial.material = m_NormalMaterial;
        m_Ainmator.SetBool(nameof(eAnimStates.Run), false);
        this.transform.localPosition = m_InitialPosition;
        this.transform.localRotation = m_InitialRotation;
        m_Ainmator.SetBool(nameof(eAnimStates.Run), false);
        m_Ainmator.SetBool(nameof(eAnimStates.Hit), false);
        m_EnemyVision.gameObject.SetActive(false);
        m_EnemyVision.isplayer = false;
        m_EnemyVision.TargetObject = null;
        CurrentHp = MaxHp;
        Bat.SetActive(false);
    }

    private void GameComplete()
    {
        m_FollowerAgent.enabled = false;
        isfollowing = false;
        islookat = false;
        m_EnemyVision.gameObject.SetActive(false);
        m_FollowerAgent.enabled = false;
        m_EnemyVision.isplayer = false;
        m_Ainmator.SetBool(nameof(eAnimStates.Run), false);
        m_Ainmator.SetBool(nameof(eAnimStates.Hit), false);
    }
    #endregion Game States


    void ApplyDamage()
    {
        CurrentHp = CurrentHp - DecreaseHp;

        if (CurrentHp <= 0)
        {
            m_FollowerAgent.enabled = false;
            m_EnemyVision.gameObject.SetActive(false);
            if (GameManager.Instance.Player.Followers.Count > 0)
            {
                var Followerdead = PoolManager.Instance.Dequeue(ePoolType.FollowerDead);
                Followerdead.transform.position = this.transform.position;
                this.gameObject.SetActive(false);
                Followerdead.SetActive(true);
                GameManager.Instance.Player.Followers.RemoveAt(0);
            }
            if (GameManager.Instance.Player.CurrentHp <= 0 && GameManager.Instance.Player.Followers.Count == 0)
            {
                GameManager.Instance.GameFail();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(nameof(eTagType.DamagePlayer)))
        {
            ApplyDamage();
        }
    }

   
  
}


