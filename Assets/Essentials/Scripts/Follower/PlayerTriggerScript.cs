using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerTriggerScript : MonoBehaviour
{
    #region Data

    [SerializeField] private FollowerScript[] m_Followers;
    #endregion Data

    #region Unity Initialization

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset += Reset;

    }

    private void OnDisable()
    {
        GameManagerDelegate.OnGameReset -= Reset;
    }
    #endregion Unity Initialization

    #region Game States
    void Reset()
    {
        for (int i = 0; i < m_Followers.Length; i++)
        {
            m_Followers[i].gameObject.SetActive(true);
            m_Followers[i].m_FollowerAgent.enabled = false;
            m_Followers[i].isfollowing = false;
            m_Followers[i].islookat = false;
            m_Followers[i].m_SkinMeshMaterial.material = m_Followers[i].m_NormalMaterial;
            m_Followers[i].m_Ainmator.SetBool(nameof(eAnimStates.Run), false);
            m_Followers[i].transform.localPosition = m_Followers[i].m_InitialPosition;
            m_Followers[i].transform.localRotation = m_Followers[i].m_InitialRotation;
            m_Followers[i].GameReset();
        }
    }

    #endregion Game States

    #region Trigger

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(nameof(eTagType.Player)))
        {
            for(int i=0;i <m_Followers.Length; i++)
            {
                if (StorageManager.Instance.Vibration == 0)
                {
                    MMVibrationManager.Haptic(HapticTypes.LightImpact);
                }
                m_Followers[i].PlayerDetected();
                GameManager.Instance.Player.Followers.Add(m_Followers[i]);
            }
        }
    }
    #endregion Trigger
}
