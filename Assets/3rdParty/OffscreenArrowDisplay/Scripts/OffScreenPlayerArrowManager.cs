﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OffScreenPlayerArrowManager : Singleton<OffScreenPlayerArrowManager>
{
    [SerializeField]
    private bool m_UseInstantiation = true;

    [SerializeField]
    private GameObject m_UIArrowsPrefab;

    [SerializeField]
    private ePoolType m_ArrowsPoolType;

    [SerializeField]
    private float m_ArrowsOffset;

    [SerializeField]
    private Vector3 m_ArrowOnScreenOffset;

    private float m_ArrowSize = 0.4f;


    private Camera m_MainCamera;
    private List<Transform> m_KnownPlayers = new List<Transform>();


    private Vector2 m_DummyOnScreenPos;
    private Vector2 m_DummyArrowPos;
    private GameObject m_DummyGameObject;

    private bool m_CanUseArrows;

    #region Unity Loop

    public  void Start()
    {
        startArrowDetecting();
        if (m_MainCamera == null)
            m_MainCamera = Camera.main;

        //if (m_UseInstantiation)
        //    Debug.LogError("Please Setup PoolManager instead!");

    }

    private void OnEnable()
    {
        GameManagerDelegate.OnGameReset+= OnReset;
        GameManagerDelegate.OnGameStart += startArrowDetecting;
        // GameManager.OnLevelReset += OnReset;

        // GameManager.OnLevelStarted += startArrowDetecting;
    }

    public void OnDisable()
    {
        //base.OnDisable();
        GameManagerDelegate.OnGameReset -= OnReset;
        GameManagerDelegate.OnGameStart -= startArrowDetecting;
       // GameManager.OnLevelReset -= OnReset;

       // GameManager.OnLevelStarted -= startArrowDetecting;
    }

    #endregion

    #region Events

    public void OnReset()
    {
        m_KnownPlayers.Clear();
        m_CanUseArrows = false;

       
        foreach (Transform child in transform)
        {
            if (m_UseInstantiation)
                Destroy(child.gameObject);
            //else
                //PoolManager.Instance.Queue(m_ArrowsPoolType, child.gameObject);
        }

       // while(transform.childCount > 0 && GameManager.Instance.GameState != eGameState.Playing)
       // {
            //PoolManager.Instance.Queue(m_ArrowsPoolType, transform.GetChild(0).gameObject);
      //  }
    }

    #endregion

    private void startArrowDetecting()
    {
        m_CanUseArrows = true;
    }

    public void ShowArrowIfPlayerOffscreen(Transform i_PlayerTransform, Color i_ArrowColor , bool i_ShowArrow = true, bool i_ShowOnScreenArrow = false)
    {
        int PlayerIndex = 0;

        if (!m_CanUseArrows)
            return;

        if (m_MainCamera == null)
            m_MainCamera = Camera.main;

        if (m_KnownPlayers.Contains(i_PlayerTransform))
        {
            PlayerIndex = m_KnownPlayers.IndexOf(i_PlayerTransform);
        }
        else
        {
            instantiateNewArrow();
            PlayerIndex = m_KnownPlayers.Count;
            m_KnownPlayers.Add(i_PlayerTransform);

            transform.GetChild(PlayerIndex).GetComponent<Image>().color = i_ArrowColor;
        }

        m_DummyOnScreenPos = m_MainCamera.WorldToScreenPoint(i_PlayerTransform.position);


        if ((m_DummyOnScreenPos.x < 0 || m_DummyOnScreenPos.x > Screen.width || m_DummyOnScreenPos.y < 0 || m_DummyOnScreenPos.y > Screen.height) && i_PlayerTransform.gameObject.activeSelf && i_ShowArrow)
        {
            transform.GetChild(PlayerIndex).gameObject.SetActive(true);
            
            m_DummyArrowPos = m_DummyOnScreenPos;
            m_DummyArrowPos.Set
                (
                    Mathf.Clamp(m_DummyArrowPos.x, 0, Screen.width),
                    Mathf.Clamp(m_DummyArrowPos.y, 0, Screen.height)
                );
          

            transform.GetChild(PlayerIndex).position = m_DummyArrowPos + Vector2.right * transform.GetChild(PlayerIndex).up * m_ArrowsOffset;
            transform.GetChild(PlayerIndex).up = (m_DummyOnScreenPos - m_DummyArrowPos).normalized;
        }
        else if (i_PlayerTransform.gameObject.activeSelf && i_ShowArrow && i_ShowOnScreenArrow)
        {
            transform.GetChild(PlayerIndex).position = RectTransformUtility.WorldToScreenPoint(m_MainCamera, i_PlayerTransform.position + m_ArrowOnScreenOffset);
            transform.GetChild(PlayerIndex).eulerAngles = new Vector3 (0,0,180);
            transform.GetChild(PlayerIndex).gameObject.SetActive(true);
        }
        else
        {
            transform.GetChild(PlayerIndex).gameObject.SetActive(false);
        }
    }

    public void HidePlayerArrow(Transform i_PlayerTransform)
    {
        if (m_KnownPlayers.Contains(i_PlayerTransform))
        {
            transform.GetChild(m_KnownPlayers.IndexOf(i_PlayerTransform)).gameObject.SetActive(false);
        }
    }

    private void instantiateNewArrow()
    {
        if(m_UseInstantiation)
            Instantiate(m_UIArrowsPrefab, transform);
        else
        {
            m_ArrowSize = 1;

            m_DummyGameObject = PoolManager.Instance.Dequeue(m_ArrowsPoolType);
            m_DummyGameObject.transform.localScale = new Vector3(m_ArrowSize, m_ArrowSize, m_ArrowSize);
            m_DummyGameObject.transform.SetParent(transform,false);
        }
    }
}
